class WordToken(str):
	pass

class SymbolToken(str):
	pass

def basic_tokenize(line):
	res = []
	word_content = ""
	for char in line:
		if char.isalpha():
			word_content += char
		else:
			if word_content:
				res.append(WordToken(word_content))
			if not char.isspace():
				res.append(SymbolToken(char))
			word_content = ""
	if word_content:
		res.append(WordToken(word_content))
	return res