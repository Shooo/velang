from string import whitespace
from textwrap import indent


def correct_split(text):
    words = []
    current_word = ""
    for letter in text:
        if letter.isspace():
            if current_word != "":
                words.append(current_word)
                current_word = ""
            continue
        if current_word == "" or (letter.isalpha() and current_word[-1].isalpha()) or (letter.isnumeric() and current_word[-1].isnumeric()):
            current_word += letter
        else:
            words.append(current_word)
            current_word = letter
    words.append(current_word)
    return words 

def does_split(text, index):
    if index < 0 or index >= len(text) - 1:
        return True
    return not (text[index].isalpha() and text[index+1].isalpha())

def starts_with_word(text, word):
    return text.startswith(word) and does_split(text, len(word)-1)

def ends_with_word(text, word):
    return text.endswith(word) and does_split(text, len(text) - len(word)-1)

def substr_match_word(text, i, word):
    return starts_with_word(text[i:], word) and ends_with_word(text[:i+len(word)], word)

def remove_indent(text):
    if text.startswith(" " * 4):
        return text[4:]
    if text.startswith("\t"):
        return text[1:]
    return text

def any_match(list, func):
    for x in list:
        if func(x):
            return x
    return None

def any_match_index(list, func):
    for i, x in enumerate(list):
        if func(x):
            return i
    return None


def find_last_except(text, to_find, **kwargs):
    if len(text) < len(to_find): return None
    
    class Counter:
        def __init__(self):
            self.count = 0

    nested_inc_count = {}
    nested_sub_count = {}
    for start, end in kwargs.get("nested", []):
        c = Counter()
        nested_inc_count[start] = c
        nested_sub_count[end] = c

    linear_wrappers = kwargs.get("linear", [])
    linear_current_presence = None

    for i in range(len(text) - len(to_find), 0, -1):
        if linear_current_presence is not None:
            linear_limit = any_match(linear_wrappers, lambda limit: substr_match_word(text, i, limit) and linear_current_presence == limit)
            if linear_limit is not None:
                linear_current_presence = None
            continue
        
        nested_start = any_match(nested_inc_count.keys(), lambda start: substr_match_word(text, i, start))
        if nested_start is not None:
            nested_inc_count[nested_start].count+=1
            continue
        
        nested_end = any_match(nested_sub_count.keys(), lambda end: substr_match_word(text, i, end))
        if nested_end is not None:
            nested_sub_count[nested_end].count-=1
            continue
        
        linear_limit = any_match(linear_wrappers, lambda limit: substr_match_word(text, i, limit))
        if linear_limit is not None:
            linear_current_presence = linear_limit
            continue

        if  not any(count.count for count in nested_inc_count.values()) \
            and substr_match_word(text, i, to_find):
            return i
    return None

def last_binary_ops_found(text, ops):
    res = []
    aux = None
    for op in ops:
        index = find_last_except(text, op.text, nested=["()"], linear="\"")
        if index is not None:
            if len(res) == 0:
                res.append(op)
                aux = index
            elif index == aux:
                res.append(op)
            elif (index < aux):
                res = [op]
                aux = index
    return res, aux

def first_unary_op_found(text, ops):
    for op in ops:
        if text.startswith(op.text) and does_split(text, len(op.text)-1):
            return op
    return None

class Operator:
    def __init__(self, text, func, types):
        self.text = text
        self.func = func
        self.types = types
    
    def valid(self, *args):
        for t, arg in zip(self.types, args):
            if type(arg) != t:
                return False
        return True
    
    def __call__(self, *args):
        return self.func(*args)

class ExecutionScope:
    def __init__(self, parent_scope = None, line_counter = 1):
        self.parent_scope = parent_scope
        self.variables = {}
        self.line_counter = line_counter
    
    def resolve_binary_ops(self, text, ops):
        ops, ind = last_binary_ops_found(text, ops)
        if len(ops) == 0: return None
        a = self.parse_expression(text[:ind].strip())
        if a is None: return None
        b = self.parse_expression(text[ind+len(ops[0].text):].strip())
        if b is None: return None
        for op in ops:
            if op.valid(a, b):
                return op(a, b)
        return None

    def resolve_unary_ops(self, text, ops):
        op = first_unary_op_found(text, ops)
        if op is None: return None
        expression_value = self.parse_expression(text[len(op.text):].strip())
        if expression_value is None: return None
        return op(expression_value)

    def parse_expression(self, text):
        if len(text) == 0:
            print(f"Error at line {self.get_line()}, expected expression but received empty text instead")
            return None
        if text.startswith("(") and text.endswith(")"):
            return self.parse_expression(text[1:-1])
        if len(text) >= 2 and text.startswith("\"") and text.endswith("\"") and "\"" not in text[1:-1]:
            return text[1:-1]
        if text == "true":
            return True
        if text == "false":
            return False
        if text.isnumeric():
            return int(text)
        if text.isalpha():
            if not self.variable_exists(text):
                print(f"Error at line {self.get_line()}, variable \"{text}\" is not declared.")
                return None
            return self.get_variable(text)
        res = self.resolve_binary_ops(text, [
            Operator(">", lambda a, b: a > b, [int, int]),
            Operator(">=", lambda a, b: a >= b, [int, int]),
            Operator("<", lambda a, b: a < b, [int, int]),
            Operator("<=", lambda a, b: a <= b, [int, int]),
            Operator("&", lambda a, b: a and b, [bool, bool]),
            Operator("|", lambda a, b: a or b, [bool, bool]),
        ])
        if res is not None: return res
        res = self.resolve_binary_ops(text, [
            Operator("+", lambda a, b: a + b, [int, int]),
            Operator("-", lambda a, b: a - b, [int, int]),
            Operator("+", lambda a, b: a + b, [str, str]),
            Operator("+", lambda a, b: a + str(b), [str, int]),
            Operator("+", lambda a, b: str(a) + b, [int, str]),
            Operator("_", lambda a, b: a + " " + b, [str, str]),
            Operator("_", lambda a, b: a + " " + str(b), [str, int]),
            Operator("_", lambda a, b: str(a) + " " + b, [int, str]),
            Operator("_", lambda a, b: str(a) + " " + str(b), [int, int])
        ])
        if res is not None: return res
        res = self.resolve_binary_ops(text, [
            Operator("*", lambda a, b: a * b, [int, int]),
            Operator("/", lambda a, b: a // b, [int, int]),
            Operator("%", lambda a, b: a % b, [int, int])
        ])
        if res is not None: return res
        res = self.resolve_binary_ops(text, [
            Operator("^", lambda a, b: a ** b, [int, int]),
        ])
        if res is not None: return res
        res = self.resolve_unary_ops(text, [
            Operator("!", lambda a: not a, [int]),
            Operator("not", lambda a: not a, [int]),
            Operator("?", lambda a: bool(a), [int]),
            Operator("+", lambda a: a, [int]),
            Operator("-", lambda a: -a, [int]),
            Operator("~", lambda a: ~a, [int])
        ])
        if res is not None: return res
        print(f"Error at line {self.get_line()}, expressions must be a number or an operation (+, -, *, /). \"{text}\" is not a valid expression.")
        return None

    def parse_inline_instruction(self, text):
        if text.startswith("print(") and text.endswith(")"):
            start = len("print(")
            end = -len(")")
            expression_value = self.parse_expression(text[start:end])
            if expression_value is not None:
                print({
                    int: expression_value,
                    bool: "true" if expression_value else "false",
                    str: expression_value
                }[type(expression_value)])
            return
        if not "=" in text:
            print(f"Error at line {self.get_line()}, instructions must be either a print or an assignement. \"{text}\" is not a valid instruction.")
            return
        var_name, expression_text = [text.strip() for text in text.split("=")]
        if not var_name.isalpha():
            print(f"Error at line {self.get_line()}, \"{var_name}\" is not a valid variable name. A variable name should be one word composed only of alphabetical characters.")
            return
        expression_value = self.parse_expression(expression_text)
        if expression_value is None:
            return
        self.set_variable(var_name, expression_value)

    def parse_instruction(self, source_code):
        text = source_code[self.line_counter-1]
        if len(text) == 0:
            return
        if text.lstrip() != text:
            print(f"Error at line {self.get_line()}, wrong indentation.")
        if starts_with_word(text, "if"):
            expression_value = self.parse_expression(text[len("if"):].strip())
            endif = any_match_index(source_code[self.line_counter:], lambda line: line.lstrip() == line)
            endelse = None
            hasEndif = endif is not None
            hasElse = False
            if not hasEndif:
                endif = len(source_code)
            else:
                endif += self.line_counter
            if hasEndif:
                if starts_with_word(source_code[endif], "else"):
                    hasElse = True
                    endelse = any_match_index(source_code[endif+1:], lambda line: line.lstrip() == line)
                    if endelse is None:
                        endelse = len(source_code)
                    else:
                        endelse += endif+1
                    if not expression_value:
                        ExecutionScope(self).read([
                            remove_indent(line)
                            for line in source_code[endif+1:endelse]
                        ])
            if expression_value:
                ExecutionScope(self).read([
                    remove_indent(line)
                    for line in source_code[self.line_counter:endif]
                ])
            if hasElse:
                self.line_counter = endelse
            else:
                self.line_counter = endif
        elif starts_with_word(text, "while"):
            endwhile = any_match_index(source_code[self.line_counter:], lambda line: line.lstrip() == line)
            if endwhile is None: endwhile = len(source_code)
            else: endwhile += self.line_counter
            while self.parse_expression(text[len("while"):].strip()):
                ExecutionScope(self).read([
                    remove_indent(line)
                    for line in source_code[self.line_counter:endwhile]
                ])
            self.line_counter = endwhile
        else:
            self.parse_inline_instruction(text)

    def read(self, source_code):
        while self.line_counter < len(source_code) + 1:
            self.parse_instruction(source_code)
            self.line_counter+=1
    
    def get_line(self):
        return self.line_counter + (0 if self.parent_scope is None else self.parent_scope.get_line()) 

    def variable_exists(self, name):
        return name in self.variables or (self.parent_scope is not None and self.parent_scope.variable_exists(name))

    def get_variable(self, name):
        if name in self.variables:
            return self.variables[name]
        if self.parent_scope is not None:
            return self.parent_scope.get_variable(name)
        return None

    def set_variable(self, name, value):
        if name not in self.variables and self.parent_scope is not None and self.parent_scope.variable_exists(name): # O(n^2) mais osef d'optimiser
            self.parent_scope.set_variable(name, value)
        self.variables[name] = value

if __name__ == "__main__":
    with open("code.vel") as file:
        ExecutionScope().read([line_text.rstrip() for line_text in file])
        