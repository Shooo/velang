# maybe:
# - write a Token class that these would inherit from
# - contain the start and end symbol in these classes
# - somehow indicate line and column number when an error happens
# - write a proper error stack
# - change every "source" parameter to something like a Cursor class?
#	tokens would be able to save their position into a sort of CursorRange
#	the cursor could be used to display the position of an error, or the code around it 
# - count alinea as a token whose start is \n, and end is any non-tab character
# - add tokens Number and Symbol..?
# - make a token aware of it's index, so that the AST later on can merge
# 	the CursorRanges of neighbouring tokens without checking for whitespaces 
# - make multiple cursor classes:
# 	- SourceCursor
#	- InterpretorCursor
#	- EvalCursor

class Cursor:
	def __init__(self, source_lines, line_pos = 0, column_pos = 0):
		self.source_lines = source_lines
		self.line_pos = line_pos
		self.column_pos = column_pos

	def shift_pos(self, offset = 1):
		if offset > 0:
			while 	self.column_pos + offset >= len(self.source_lines[self.line_pos]) \
			and 	self.line_pos < len(self.source_lines) - 1:
				offset -= len(self.source_lines[self.line_pos]) - self.column_pos
				self.line_pos += 1
				self.column_pos = 0
		elif offset < 0:
			while	self.column_pos + offset < 0 \
			and		self.line_pos > 0:
				offset -= self.column_pos
				self.line_pos -= 1		
				self.column_pos = len(self.source_lines[self.line_pos]) - 1
		self.column_pos = min(max(
			0,
			self.column_pos + offset),
			len(self.source_lines[self.line_pos]) - 1
		)
		
	def set_pos(self, line_pos, column_pos):
		self.line_pos = line_pos
		self.column_pos = column_pos

	def __equals__(self, other):
		return 	self.source_lines	== other.source_lines \
		and		self.line_pos		== other.line_pos \
		and		self.column_pos		== other.column_pos

	def __lt__(self, other):
		return (self.line_pos, self.column_pos) < (other.line_pos, other.column_pos)

	def __add__(self, other):
		cursor = Cursor.from_cursor(self)
		cursor.shift_pos(other)
		return cursor

	def __sub__(self, other):
		cursor = Cursor.from_cursor(self)
		cursor.shift_pos(-other)
		return cursor

	def startswith(self, text):
		return self.get_text(len(text)).startswith(text)

	def get_text(self, length = 1):
		return self.text_between(self + length, True)	

	def text_between(self, other, include_first = False, include_last = False):
		if self.source_lines != other.source_lines:
			return ""
		if self.line_pos == other.line_pos:
			return self.source_lines[self.line_pos][
				min(self.column_pos, other.column_pos):
				max(self.column_pos, other.column_pos)
			]
		first, second = sorted((self, other))
		return 	self.source_lines[first.line_pos][first.column_pos:] \
			+	"\n".join(line for line in self.source[not include_first + first.line_pos:include_last + second.column_pos]) \
			+	self.source_lines[second.line_pos][:second.column_pos]

	@classmethod
	def from_cursor(cls, cursor):
		return cls(
			cursor.source_lines,
			cursor.line_pos,
			cursor.column_pos
		)
	
	@classmethod
	def from_source_start(cls, source_lines):
		return cls(
			source_lines,
			0,
			0
		)

	@classmethod
	def from_source_end(cls, source_lines):
		return cls(
			source_lines,
			len(source_lines) - 1,
			len(source_lines[-1]) -1
		)
	

class Token:
	def __init__(self, start_cursor, end_cursor):
		self.start_cursor = start_cursor
		self.end_cursor = end_cursor
		self.content = (start_cursor + self.start_offset()).text_between(end_cursor)

	def __str__(self):
		return self.content
		
	@classmethod
	def starts(cls, source):
		return False
	
	@classmethod
	def cancels(cls, source):
		return False

	@classmethod
	def ends(cls, source):
		return False

	@classmethod
	def start_offset(cls):
		return 0

	@classmethod
	def cancel_offset(cls):
		return 0

	@classmethod
	def end_offset(cls):
		return 0

	def __str__(self):
		return (self.start_cursor + self.start_offset).text_between(self.end_cursor - self.end_offset())

	start_symbols = []
	end_symbols = []
	# TODO: make the Token class aware of it's child classes

def create_symbol_delimited_token_class(start_symbol, end_symbol = None):
	Token.start_symbols.append(start_symbol)
	if end_symbol is not None:
		Token.end_symbols.append(end_symbol)
	
	class SymbolDelimitedToken(Token):
		@classmethod
		def starts(cls, source):
			return source.startswith(start_symbol)

		@classmethod
		def ends(cls, source):
			if end_symbol is None:
				return super().ends(source)
			if len(source) == 0:
				cls.missing_end()
			return source.startswith(end_symbol)

		@classmethod
		def start_offset(cls):
			return len(start_symbol)

		@classmethod
		def end_offset(cls):
			if end_symbol is None:
				return super().end_offset()
			return len(end_symbol)
		
		@classmethod
		def missing_end(cls):
			print(f"Missing {end_symbol}: the {cls.__name__} doesn't end.")
			
	return SymbolDelimitedToken

class Word(Token):
	@classmethod
	def starts(cls, source):
		return 	source[0:1].isalpha() \
		and	not	any(
					source.startswith(start_symbol)
					for start_symbol in Token.start_symbols
				)

	@classmethod
	def ends(cls, source):
		if len(source) == 0:
			return False
		return 	not source[0].isalpha() \
				or 	any(
						source.startswith(start_symbol)
						for start_symbol in Token.start_symbols
					)

class Alinea(create_symbol_delimited_token_class("\n")):		
	def __init__(self, content):
		super().__init__(content)
		self.offset = 0
		for character in content:
			if character != "\t":
				break
			self.offset += 1

	@classmethod
	def cancels(cls, source):
		return source[0:1].startswith("\n")
	
	@classmethod
	def ends(cls, source):
		if len(source) == 0:
			return False
		return not source[0].isspace()

	@classmethod
	def missing_end(cls):
		print("Missing \" (quote): the string doesn't end.")


class String(create_symbol_delimited_token_class(*'""')):
	pass

class Comment(create_symbol_delimited_token_class("/*", "*/")):
	pass

class Tokeniser:
	"""
	Class used by the velang language to turn source code into tokens.
	There are currently 3 types of tokens:
	- Strings:
		String litterals.
		Comprised of characters between "" (quotes), and not inside of Comments
	- Comments:
		For now block comments only.
		For characters between /* and */, and not inside of Strings
	- Words:
		Basicly everything else.
		Represents the "real" code: variables, operators, keywords...
	"""

	token_classes = [Word, Alinea, String, Comment]

	@classmethod
	def tokenize(cls, source_lines):
		tokens = []
		index = 0
		cursor = Cursor.from_source_start(source_lines)
		while cursor != Cursor.from_source_end(source_lines):
			if 	cursor.get_text().isspace() \
			and not cursor[index] == "\n":
				index+=1
				continue

			for token_class in Tokeniser.token_classes:
				if token_class.starts(source[index:]):
					content_start = index + token_class.start_offset()
					index = content_start
					
					cancels = token_class.cancels(source[index:])
					ends = token_class.ends(source[index:])
					while not cancels and not ends and index < len(source)-1:
						index += 1
						cancels = token_class.cancels(source[index:])
						if cancels:
							break
						ends = token_class.ends(source[index:])

					if cancels:
						index += token_class.cancel_offset()
						break
					if ends:
						content_end = index
						tokens.append(
							token_class(
								source[content_start:content_end]
							)
						)
						index += token_class.end_offset()
						break

		return tokens

	@classmethod
	def cut_word(cls, source):
		"""
		Find the end of the Word token, according to splits.
		Returns the string that the word token should cover.
		"""
		# just like source[0], except it gives "" if out of range
		token_str = source[0:1]
		for index in range(1, len(source)):
			if Tokeniser.does_split(source[index-1:]):
				break
			token_str += source[index]
		return token_str


def tokeniser_test():
	with open("tokeniser_test.vel") as file:
		source_lines = file.readlines()
	print(source_lines)

	tokens = Tokeniser.tokenize(source_lines)
	print("\n".join(f"({type(token).__name__}) \"{token}\"" for token in tokens))

def cursor_test():
	with open("tokeniser_test.vel") as file:
		source_lines = file.readlines()
	print(source_lines)
	cursor = Cursor(source_lines)
	print()
	print_cursor(source_lines, cursor)
	cursor.shift_pos(5)
	print()
	print_cursor(source_lines, cursor)
	cursor.shift_pos(6)
	print()
	print_cursor(source_lines, cursor)
	cursor.shift_pos(-15)
	print()
	print_cursor(source_lines, cursor)

def text_between_test():
	with open("tokeniser_test.vel") as file:
		source_lines = file.readlines()
	cursor = Cursor(source_lines)
	print((cursor + 2).text_between(cursor + 8))

def print_cursor(source_lines, cursor):
	for line_pos, line in enumerate(source_lines):
		line = line.rstrip("\n")
		print(line)
		if cursor.line_pos == line_pos:
			print("-" * cursor.column_pos + "^" + "-" * (len(line) - cursor.column_pos - 1))
		else:
			print("-" * len(line))

tokeniser_test()
