from compiler.parser import *
from velang.concrete_syntax_tree import *

def velang_syntax():
	graph = ParsingGraph(CompilationContext())
	
	graph.add_node(
		"script", CST_script,
		MatchRepetition(
			MatchSequence(
				MatchTokenContent("3+3;"),
				MatchTokenType("NEWLINE")
			),
		),
		[]
	)
	graph.add_node(
		"statement", CST_statement,
		MatchSequence(
			MatchParsingNode(graph, "expression"),
			MatchTokenContent(";")
		),
		[]
	)
	graph.add_node(
		"expression", None,
		None,
		["addition"]
	)



	graph.add_node(
		"addition", CST_addition,
		MatchSequence(
			MatchParsingNode(graph, "rvalue"),
			MatchTokenContent("+"),
			MatchParsingNode(graph, "addition")
		),
		["soustraction"]
	)
	graph.add_node(
		"soustraction", CST_addition,
		MatchSequence(
			MatchParsingNode(graph, "addition"),
			MatchTokenContent("+"),
			MatchParsingNode(graph, "multiplication")
		),
		["multiplication"]
	)
	
	
	
	graph.add_node(
		"multiplication", CST_multiplication,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("*"),
			MatchParsingNode(graph, "power")
		),
		["division"]
	)
	graph.add_node(
		"division", CST_division,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("/"),
			MatchParsingNode(graph, "power")
		),
		["integer_division"]
	)
	graph.add_node(
		"integer_division", CST_integer_division,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("//"),
			MatchParsingNode(graph, "power")
		),
		["modulo"]
	)
	graph.add_node(
		"modulo", CST_modulo,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("%"),
			MatchParsingNode(graph, "power")
		),
		["power"]
	)




	graph.add_node(
		"power", CST_power,
		MatchSequence(
			MatchParsingNode(graph, "power"),
			MatchTokenContent("**"),
			MatchParsingNode(graph, "positive")
		),
		["positive"]
	)
	
	

	graph.add_node(
		"positive", CST_positive,
		MatchSequence(
			MatchTokenContent("+"),
			MatchParsingNode(graph, "positive")
		),
		["negative"]
	)
	graph.add_node(
		"negative", CST_negative,
		MatchSequence(
			MatchTokenContent("-"),
			MatchParsingNode(graph, "positive")
		),
		["rvalue"]
	)


	graph.add_node(
		"rvalue", None,
		None,
		[
			"number_constant",
			"variable_expression"
		]
	)
	graph.add_node(
		"number_constant", CST_number_constant,
		MatchTokenType("NUMBER"),
		[]
	)
	graph.add_node(
		"variable_expression", CST_variable_expression,
		MatchTokenType("WORD"),
		[]
	)

	return graph


def ebnf_syntax():
	graph = ParsingGraph(CompilationContext())
	
	graph.add_node(
		"script", CST_script,
		MatchRepetition(
			MatchSequence(
				MatchTokenContent("3+3;"),
				MatchTokenType("NEWLINE")
			),
		),
		[]
	)
	graph.add_node(
		"statement", CST_statement,
		MatchSequence(
			MatchParsingNode(graph, "expression"),
			MatchTokenContent(";")
		),
		[]
	)
	graph.add_node(
		"expression", None,
		None,
		["addition"]
	)



	graph.add_node(
		"addition", CST_addition,
		MatchSequence(
			MatchParsingNode(graph, "rvalue"),
			MatchTokenContent("+"),
			MatchParsingNode(graph, "addition")
		),
		["soustraction"]
	)
	graph.add_node(
		"soustraction", CST_addition,
		MatchSequence(
			MatchParsingNode(graph, "addition"),
			MatchTokenContent("+"),
			MatchParsingNode(graph, "multiplication")
		),
		["multiplication"]
	)
	
	
	
	graph.add_node(
		"multiplication", CST_multiplication,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("*"),
			MatchParsingNode(graph, "power")
		),
		["division"]
	)
	graph.add_node(
		"division", CST_division,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("/"),
			MatchParsingNode(graph, "power")
		),
		["integer_division"]
	)
	graph.add_node(
		"integer_division", CST_integer_division,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("//"),
			MatchParsingNode(graph, "power")
		),
		["modulo"]
	)
	graph.add_node(
		"modulo", CST_modulo,
		MatchSequence(
			MatchParsingNode(graph, "multiplication"),
			MatchTokenContent("%"),
			MatchParsingNode(graph, "power")
		),
		["power"]
	)




	graph.add_node(
		"power", CST_power,
		MatchSequence(
			MatchParsingNode(graph, "power"),
			MatchTokenContent("**"),
			MatchParsingNode(graph, "positive")
		),
		["positive"]
	)
	
	

	graph.add_node(
		"positive", CST_positive,
		MatchSequence(
			MatchTokenContent("+"),
			MatchParsingNode(graph, "positive")
		),
		["negative"]
	)
	graph.add_node(
		"negative", CST_negative,
		MatchSequence(
			MatchTokenContent("-"),
			MatchParsingNode(graph, "positive")
		),
		["rvalue"]
	)


	graph.add_node(
		"rvalue", None,
		None,
		[
			"number_constant",
			"variable_expression"
		]
	)
	graph.add_node(
		"number_constant", CST_number_constant,
		MatchTokenType("NUMBER"),
		[]
	)
	graph.add_node(
		"variable_expression", CST_variable_expression,
		MatchTokenType("WORD"),
		[]
	)

	return graph