from compiler.tokenizer import *

class CompilationContext:
	def __init__(self):
		self.warnings = []
		self.error = None

class ExecutionContext:
	def __init__(self, parent = None):
		self.parent = parent
		self.variables = {}
		self.warnings = []
		self.error = None

	def get_variable(self, name):
		scope = self
		while scope.parent is not None:
			scope = scope.parent
			if name in scope.variables:
				break
		else:
			self.error = f"Variable {name} used but undeclared."
			return None
		return scope.variables[name]

	def set_variable(self, name, value):
		scope = self
		while scope.parent is not None:
			scope = scope.parent
			if name in scope.variables:
				break
		else:
			scope = self
		scope.variables[name] = value


class CST_Node:
	def __init__(self, name, children):
		self.name = name
		self.children = children
		self.params = [
			child
			for child in self.children
			if isinstance(child, CST_Node)
		]

	def __str__(self):
		return self.name + "".join(
			("\n|-- ", "\n`-- ")[i == len(self.children)-1] + "".join(
				("|   ", "    ")[i == len(self.children)-1].join(
					str(child).splitlines(True)
				)
			)
			for i, child in enumerate(self.children)
		)

class CST_transition(CST_Node):
	def __init__(self, compilation_context, name, children, params_optionnal):
		super().__init__(name, children)
		if len(self.params) == 0 and not params_optionnal:
			compilation_context.error = \
				f"The transition CST node \"{name}\" received no parameter," \
				"even though the parameter is not optionnal." \
				"Please change the associated parsing rules, or the nature of the node."
		elif len(self.params) > 1:
			compilation_context.error = \
				f"The transition CST node \"{name}\" received multiple parameters but it only supports one." \
				"Please change the associated parsing rules, or the nature of the node."
		
	def execute(self, context):
		return self.params[0].execute(context)

class CST_multiple(CST_Node):
	def __init__(self, compilation_context, name, children, params_optionnal):
		super().__init__(name, children)
		if len(self.params) == 0 and not params_optionnal:
			compilation_context.error = \
				f"The transition CST node \"{name}\" received no parameters" \
				"even though they're not optionnal." \
				"Please change the associated parsing rules, or the nature of the node."

	def execute(self, context):
		for param in self.params:
			param.execute(context)
			if context.error is not None:
				return None

class CST_script(CST_multiple):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "script", children, True)

class CST_statement(CST_transition):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "statement", children, True)


class CST_unary_operator(CST_Node):
	def __init__(self, compilation_context, name, children):
		super().__init__(name, children)
		if len(self.params) == 0:
			compilation_context.error = \
				f"The unary CST node \"{name}\" received no parameters." \
				"Please change the associated parsing rules, or the nature of the node."
		elif len(self.params) > 1:
			compilation_context.error = \
				f"The unary CST node \"{name}\" received too many parameters." \
				"Please change the associated parsing rules, or the nature of the node."
	
	def execute(self, context, func):
		only = self.params[0].execute(context)
		if context.error is not None:
			return None
		return func(only)


class CST_binary_operator(CST_Node):
	def __init__(self, compilation_context, name, children):
		super().__init__(name, children)
		if len(self.params) == 0:
			compilation_context.error = \
				f"The binary CST node \"{name}\" received no parameters." \
				"Please change the associated parsing rules, or the nature of the node."
		elif len(self.params) == 1:
			compilation_context.error = \
				f"The binary CST node \"{name}\" received only 1 parameter." \
				"Please change the associated parsing rules, or the nature of the node."
		elif len(self.params) > 2:
			compilation_context.error = \
				f"The binary CST node \"{name}\" received too many parameters." \
				"Please change the associated parsing rules, or the nature of the node."
	
	def execute(self, context, func):
		first = self.params[0].execute(context)
		if context.error is not None:
			return None
		second = self.params[1].execute(context)
		if context.error is not None:
			return None
		return func(first, second)


class CST_addition(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "addition", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a+b)

class CST_soustraction(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "soustraction", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a-b)



class CST_multiplication(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "multiplication", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a*b)
	

class CST_division(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "division", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a/b)


class CST_integer_division(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "integer_division", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a//b)


class CST_modulo(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "modulo", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a%b)




class CST_power(CST_binary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "power", children)

	def execute(self, context):
		return super().execute(context, lambda a, b: a**b)



class CST_positive(CST_unary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "positive", children)

	def execute(self, context):
		return super().execute(context, lambda a: +a)

class CST_negative(CST_unary_operator):
	def __init__(self, compilation_context, children):
		super().__init__(compilation_context, "negative", children)

	def execute(self, context):
		return super().execute(context, lambda a: -a)




class CST_variable_expression(CST_Node):
	def __init__(self, compilation_context, children):
		super().__init__("variable_expression", children)
		if len(self.params) > 0:
			compilation_context.error = \
				f"The CST node variable expression received parameters." \
				"Please change the associated parsing rules."
			return
		variables = [
			child.content
			for child in self.children
			if type(child) == Token and child.token_type == "WORD"
		]
		if len(variables) == 0:
			compilation_context.error = \
				f"The CST node variable expression received no word token." \
				"Please change the associated parsing rules."
			return
		if len(variables) > 1:
			compilation_context.error = \
				f"The CST node variable expression received multiple word tokens." \
				"Please change the associated parsing rules."
			return
		self.variable = variables[0]

	def execute(self, context):
		return context.get_variable(self.variable)

class CST_number_constant(CST_Node):
	def __init__(self, compilation_context, children):
		super().__init__("number_constant", children)
		if len(self.params) > 0:
			compilation_context.error = \
				f"The CST node number constant received parameters." \
				"Please change the associated parsing rules."
			return
		numbers = [
			child.content
			for child in self.children
			if type(child) == Token and child.token_type == "NUMBER"
		]
		if len(numbers) == 0:
			compilation_context.error = \
				f"The CST node number constant received no number token." \
				"Please change the associated parsing rules."
			return
		if len(numbers) > 1:
			compilation_context.error = \
				f"The CST node variable expression received multiple number tokens." \
				"Please change the associated parsing rules."
			return
		self.number = numbers[0]

	def execute(self, context):
		return self.number
