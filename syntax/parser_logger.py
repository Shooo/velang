
class Logger:
	def __init__(self, parent = None):
		self.parent = parent
		self.children = []

	def sub_logger(self):
		child = Logger(self)
		self.children.append(child)
		return child 
	
	def log(self, *args):
		self.children.append(" ".join(map(str, args)))
	
	def __str__(self):
		return "\n".join(
			line
			for child in self.children
			for line in (
				[child]
				if type(child) == str 
				else [
					"\t" + line
					for line in str(child).splitlines()
				] 
			)
		)

class ParserLogger:
	def __init__(self, parent = None):
		self.parent = parent
		self.children = []
		self.name = ""
		self.match = None

	def sub_logger(self):
		child = ParserLogger(self)
		self.children.append(child)
		return child 
	
	def log(self, *args):
		self.children.append(" ".join(map(str, args)))
	
	def __str__(self):
		return "\n".join([self.name] + [
			line
			for child in self.children
			for line in (
				[child]
				if type(child) == str 
				else [
					"\t" + line
					for line in str(child).splitlines()
				] 
			)
		])
	
	def winning_str(self):
		return "\n".join([self.name] + [
			line
			for child in self.children
			for line in (
				[child]
				if type(child) == str 
				else [
					"\t" + line
					for line in child.winning_str().splitlines()
				] 
			)
			if type(child) == str or child.match
		])


if __name__ == "__main__":
	logger = ParserLogger()
	logger.name = "hello"
	sub = logger.sub_logger()
	sub.log("world")
	sub.match = False
	logger.log("!")
	print(logger.winning_str())