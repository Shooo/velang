from tokenizer import tokenize

def interpreter():
	while True:
		content = input(">>> ")
		print(tokenize(content))

def tokenize_file(path):
	with open(path) as file:
		content = file.read()
		print(tokenize(content))

if __name__ == "__main__":
	import sys

	args = sys.argv[1:]
	if len(args) == 0:
		interpreter()
	elif len(args) == 1:
		tokenize_file(args[0])
	else:
		print("Too many arguments. You should either enter a file path, or nothing.")
	 
