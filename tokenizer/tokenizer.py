# Another tokenizer... this time I have a bit more hindsight about what it should do
# ... I think

# Token types include:
# - WORD
# - SYMBOL
# - NUMBER
# - NEWLINE
# - INDENT
# - DEDENT

class Token:

	def __init__(self, token_type, content):
		self.token_type = token_type
		self.content = content

	def __repr__(self):
		return f"[{self.token_type}] " + {"\n":"\\n"}.get(*[f"{self.content}"]*2)
	
	def __eq__(self, other):
		return (self.token_type, self.content) == (other.token_type, other.content)


def tokenize(source):
	# hardcoded, I know
	indentation = 0
	tokens = []

	index = 0
	while index < len(source):
		if source[index] == "\n":
			tokens.append(Token("NEWLINE", "\n"))
			current_indent = 0
			index += 1
			while index < len(source) and source[index] == "\t":
				current_indent+=1
				index += 1
			if current_indent > indentation:
				tokens.extend(Token("INDENT", "") for _ in range(current_indent - indentation))
			elif current_indent < indentation:
				tokens.extend(Token("DEDENT", "") for _ in range(indentation - current_indent))
			indentation = current_indent
		elif source[index].isspace():
			index+=1
		elif source[index].isalpha():
			current_word = source[index]
			index+=1
			while index < len(source) and source[index].isalpha():
				current_word += source[index]
				index+=1
			tokens.append(Token("WORD", current_word))
		elif source[index].isnumeric():
			current_number = source[index]
			index+=1
			while index < len(source) and source[index].isnumeric():
				current_number += source[index]
				index+=1
			tokens.append(Token("NUMBER", int(current_number)))
		else:
			tokens.append(Token("SYMBOL", source[index]))
			index+=1
	return tokens