from tokenizer import tokenize, Token

# Words, whitespaces and word splits
assert tokenize("Hello") == [Token("WORD", "Hello")], "\"Hello\" should produce a single word token"
assert tokenize("HelloWorld") == [Token("WORD", "HelloWorld")], "\"HelloWorld\" should produce a single word token"
assert tokenize(" HelloWorld") == [Token("WORD", "HelloWorld")], "\" HelloWorld\" should produce a single word token"
assert tokenize("Hello World") == [Token("WORD", "Hello"), Token("WORD", "World")], "\"Hello World\" should produce two words token"
assert tokenize("Hello  World") == [Token("WORD", "Hello"), Token("WORD", "World")], "\"Hello  World\" should produce two words token"
assert tokenize("Hello\tWorld") == [Token("WORD", "Hello"), Token("WORD", "World")], "\"Hello\tWorld\" should produce two words token"
assert tokenize("Hello\nWorld") == [Token("WORD", "Hello"), Token("NEWLINE", "\n"), Token("WORD", "World")]
assert tokenize("Hello1World") == [Token("WORD", "Hello"), Token("NUMBER", 1), Token("WORD", "World")]
assert tokenize("Hello_World") == [Token("WORD", "Hello"), Token("SYMBOL", "_"), Token("WORD", "World")]

# Numbers
assert tokenize("123") == [Token("NUMBER", 123)], "\"123\" should produce a single number token"
assert tokenize("12 3") == [Token("NUMBER", 12), Token("NUMBER", 3)], "\"123\" should produce two number tokens"
assert tokenize("12.3") == [Token("NUMBER", 12), Token("SYMBOL", "."), Token("NUMBER", 3)], "\"12.3\" should be considered separate numbers, not a float"
assert tokenize("12,3") == [Token("NUMBER", 12), Token("SYMBOL", ","), Token("NUMBER", 3)], "\"12,3\" should be considered separate numbers, not a float"

# Newlines, indent and dedent
assert tokenize("\n") == [Token("NEWLINE", "\n")], "\"\\n\" should produce a single newline token"
assert tokenize("\n\n") == [Token("NEWLINE", "\n"), Token("NEWLINE", "\n")], "\"\\n\\n\" should produce two newline token"
assert tokenize("\n\t") == [Token("NEWLINE", "\n"), Token("INDENT", "")], "\"\\n\" should produce a newline and an indent"
# Indent edge cases: (might be changed later on)
# first indent doesn't produce an indent
assert tokenize("\t\n") == [Token("NEWLINE", "\n")], "\"\\t\\n\" should not produce a starting indent"
# whitespace breaks indentation counting
assert tokenize("\n \t") == [Token("NEWLINE", "\n")], "\"\\n \\t\" should not be counted as an indent"
# Dedent
assert \
	tokenize("\n\t\n") == [Token("NEWLINE", "\n"), Token("INDENT", ""), Token("NEWLINE", "\n"), Token("DEDENT", "")], \
	"\"\\n\\t\\n\" should produce a both indent and dedent"
