from tokenizer.tokenizer import tokenize

def flatten(arr):
	return [el for line in arr for el in line]

def sum_or_none(arr):
	sum = 0
	for val in arr:
		if val is None:
			return None
		sum += val
	return sum

def min_or_none(arr):
	min = None
	for val in arr:
		if val is None:
			return None
		if min is None or val < min:
			min = val
	return min

def max_or_none(arr):
	max = None
	for val in arr:
		if val is None:
			return None
		if max is None or val > max:
			max = val
	return max

class MatchResult:
	def __init__(self, result, elements, size_consumed):
		self.result = result
		self.elements = elements
		self.size_consumed = size_consumed
	
	def __bool__(self):
		return self.result
	
	@classmethod
	def valid(cls, elements, size_consumed):
		return cls(True, elements, size_consumed)

	@classmethod
	def invalid(cls):
		return cls(False, [], 0)


class MatchSequence:
	def __init__(self, *matchers):
		# separate matchers into three categories:
		# start, middle and end
		#
		# start and end matchers are constant-size matchers from both sides
		# middle matcher have a variable size
		for i, matcher in enumerate(matchers):
			if not (matcher.min_size == matcher.max_size != None):
				break
		middle_start = i

		for i, matcher in list(enumerate(matchers))[::-1]:
			if not (matcher.min_size == matcher.max_size != None):
				break
		middle_end = i+1

		self.start_matchers = matchers[:middle_start]
		self.middle_matchers = matchers[middle_start:middle_end]
		self.end_matchers =  matchers[middle_end:]
		print(self.end_matchers)

		# compute sizes
		# the size of a matcher is the number of tokens it consumes
		self.min_size = sum_or_none(matcher.min_size for matcher in matchers)
		self.max_size = sum_or_none(matcher.max_size for matcher in matchers)
		self.start_size = sum(matcher.min_size for matcher in self.start_matchers)
		self.end_size = sum(matcher.min_size for matcher in self.end_matchers)
		
		self.min_middle_seq_size = sum(
			matcher.min_size
			for matcher in self.middle_matchers
			if matcher.min_size is not None
		)
		self.min_seq_size = self.start_size + self.min_middle_seq_size + self.end_size
	
	def parse_tokens_check_size(self, tokens, consume_all = True):
		# less tokens than the min_size taken by the known size matchers
		if len(tokens) < self.min_seq_size:
			return False
		
		# more tokens to consume than the allowed max size
		if consume_all and self.max_size is not None and len(tokens) > self.max_size:
			return False
		return True

	def parse_tokens_consume_start(self, tokens):
		elements = []
		size_consumed = 0
		for matcher in self.start_matchers:
			# not using consume_all because they always consume the same anways
			matcher_result = matcher.parse_tokens(tokens[size_consumed:][:matcher.min_size])
			if not matcher_result:
				return MatchResult.invalid()
			elements.extend(matcher_result.elements)
			size_consumed += matcher.min_size # constant size for start matchers anyways
		return MatchResult.valid(elements, self.start_size)

	def parse_tokens_consume_end(self, tokens, consume_all):
		# I could probably fuse consumed end and consume start in a single function
		if not consume_all:
			return MatchResult.valid([], 0)
		elements = []
		size_consumed = 0
		for matcher in self.end_matchers[::-1]:
			matcher_result = matcher.parse_tokens(
				# I had to use len instead of negative indexing because -0 woudldn't include the last one 
				tokens[len(tokens) - size_consumed - matcher.min_size:len(tokens) - size_consumed]
				# not using consume_all because they always consume the same anways
			)
			if not matcher_result:
				return MatchResult.invalid()
			elements.extend(matcher_result.elements)
			size_consumed += matcher.min_size # constant size for end matchers anyways
		return MatchResult.valid(elements, self.end_size)

	def parse_tokens_consume_middle(self, tokens, consume_all):
		# Refactorize this piece of code later

		token_index = 0
		# technically not needed since we adjust matcher_sizes's length each time
		matcher_index = 0

		matchers = self.middle_matchers
		# if we don't consume everything, the end matchers left-align and become part of the middle
		if not consume_all:
			# += instead of extends because I want to create a separate list,
			# to not change self.middle_matchers
			matchers += self.end_matchers

		# needed since we need to discard elements per matcher
		matcher_sizes = []
		elements_by_matcher = [[] for _ in range(len(matchers))]

		while matcher_index < len(matchers) :
			matcher = matchers[matcher_index]
			# reinitialize max size
			if matcher_index == len(matcher_sizes):
				corrected_min_middle_seq_size = (
					self.min_middle_seq_size
					if consume_all else
					self.min_middle_seq_size + self.end_size 
				)
				matcher_sizes.append(
					matcher.max_size
					if matcher.max_size is not None else
					len(tokens) - corrected_min_middle_seq_size
				)
			matcher_result = matcher.parse_tokens(
				tokens[token_index:][:matcher_sizes[matcher_index]],
				consume_all and matcher_index == len(matchers) - 1
			)
			if matcher_result:
				# if it matches, jump to the next matcher in the sequence
				elements_by_matcher[matcher_index] = matcher_result.elements
				matcher_sizes[matcher_index] = matcher_result.size_consumed
				token_index += matcher_result.size_consumed
				matcher_index += 1
			else:
				# if it didn't match, go back to the previous matcher and *size it down*
				token_index -= matcher_sizes[matcher_index - 1]
				matcher_sizes.pop()
				matcher_index-=1
				# in case the previous matcher is already zero-sized, we want the one before it
				# do...while to take the last preceding non-zero length matcher
				# yes, a very specific edge case
				while matcher_index >= 0 and matcher_sizes[matcher_index] == 0:
					token_index -= matcher_sizes[matcher_index - 1]
					matcher_sizes.pop()
					matcher_index-=1
				# all the previous matchers reached their minimum size but couldn't match
				if matcher_index < 0:
					return MatchResult.invalid()
				# don't forget to shrink the previous matcher!
				matcher_sizes[matcher_index] -= 1
		return MatchResult.valid(
			flatten(elements_by_matcher), sum(matcher_sizes)
		)

	def parse_tokens(self, tokens, consume_all = True):
		"""
		Note: consume_all indicates wheter the end fixed-size matchers align left or right
		aligning right forces to consume all
		aligning left just appends them naturally to the end of the middle matchers
		"""
		if not self.parse_tokens_check_size(tokens, consume_all):
			# wrong size
			return MatchResult.invalid()

		start_match_result = self.parse_tokens_consume_start(tokens[:self.start_size])
		if not start_match_result:
			# start doesn't match
			return MatchResult.invalid()

		end_match_result = self.parse_tokens_consume_end(tokens[len(tokens) - self.end_size:], consume_all)
		if not end_match_result:
			# end doesn't match
			return MatchResult.invalid()
		
		middle_match_result = self.parse_tokens_consume_middle(
			tokens[self.start_size:len(tokens) - self.end_size]
			if consume_all else
			tokens[self.start_size:], 
			consume_all
		)
		if not middle_match_result:
			# middle doesn't match
			return MatchResult.invalid()
		return MatchResult.valid(
			start_match_result.elements + middle_match_result.elements + end_match_result.elements,
			start_match_result.size_consumed + middle_match_result.size_consumed + end_match_result.size_consumed
		)

	def __str__(self):
		return "Sequence" + "".join(
			("\n|-- ", "\n`-- ")[i == len(self.matchers)-1] + "".join(
				("|   ", "    ")[i == len(self.matchers)-1].join(
					str(child).splitlines(True)
				)
			)
			for i, child in enumerate(self.matchers)
		)

class MatchAny:
	def __init__(self, *matchers):
		self.matchers = matchers
	
		self.min_size = min_or_none(matcher.min_size for matcher in self.matchers) 
		self.max_size = max_or_none(matcher.max_size for matcher in self.matchers) 

	def parse_tokens(self, tokens, consume_all = True):
		for matcher in self.matchers:
			match_result = matcher.parse_tokens(tokens, consume_all)
			if match_result:
				return match_result 
		return MatchResult.invalid()
	
	def __str__(self):
		return "Any" + "".join(
			("\n|-- ", "\n`-- ")[i == len(self.matchers)-1] + "".join(
				("|   ", "    ")[i == len(self.matchers)-1].join(
					str(child).splitlines(True)
				)
			)
			for i, child in enumerate(self.matchers)
		)


class MatchTokenType:
	def __init__(self, token_type):
		self.token_type = token_type
	
		self.min_size = 1
		self.max_size = 1
	
	def parse_tokens(self, tokens, consume_all = False):
		# could be fused into a less statements, but that would be a pain to read
		if len(tokens) == 0:
			return MatchResult.invalid()
		if consume_all and len(tokens) > 1:
			return MatchResult.invalid()
		if tokens[0].token_type != self.token_type:
			return MatchResult.invalid()
		return MatchResult.valid([tokens[0]], 1)

	def __str__(self):
		return f"TokenType [{self.token_type}]"

class MatchTokenContent:
	def __init__(self, token_content):
		self.contents = [
			token.content
			for token in tokenize(token_content)
		]

		self.min_size = len(self.contents)
		self.max_size = len(self.contents)


	def parse_tokens(self, tokens, consume_all = False):
		if len(tokens) < len(self.contents):
			return MatchResult.invalid()
		if consume_all and len(tokens) > len(self.contents):
			return MatchResult.invalid()
		for content, token in zip(self.contents, tokens):
			if token.content != content:
				return MatchResult.invalid()
		# return MatchResult.valid([], len(self.contents)) # if we want to discard the matching tokens
		return MatchResult.valid(tokens[:len(self.contents)], len(self.contents)) # if we want to keep the matching tokens

	def __str__(self):
		return f"TokenContent [" + "".join(str(content) for content in self.contents) + "]"

class MatchParsingNode:
	def __init__(self, graph, node_name):
		self.graph = graph
		self.node_name = node_name

		self.min_size = None
		self.max_size = None

	def parse_tokens(self, tokens, consume_all = False):
		match_result = self.graph.nodes[self.node_name].parse_tokens(tokens, consume_all)
		# to introduce tree depth...?
		if match_result:
			return MatchResult.valid([(self.node_name, match_result.elements)], match_result.size_consumed)
		return MatchResult.invalid()

	def __str__(self):
		return f"ParsingNode ({self.node_name})"

class MatchRepetition:
	# matches 1-n
	def __init__(self, matcher):
		self.matcher = matcher
	
		self.min_size = self.matcher.min_size
		self.max_size = None


	def parse_tokens(self, tokens, logger, consume_all = False):
		# right now decides to stack infinitely if 0-sized matcher matches
		# which actually isn't so bad, it makes sense
		# also, it's intended not to shrink down
		elements = []
		length = 0
		match_result = self.matcher.parse_tokens(tokens)
		if not match_result:
			return MatchResult.invalid()
		elements.extend(match_result.elements)
		length += match_result.size_consumed

		while match_result:
			match_result = self.matcher.parse_tokens(tokens[length:])
			if match_result:
				elements.extend(match_result.elements)
				length += match_result.size_consumed
		
		if consume_all and len(tokens) != length:
			return MatchResult.invalid()
		return MatchResult.valid(elements, length)

	def __str__(self):
		return f"Repetition " + str(self.matcher)
		
class ParsingGraph:
	def __init__(self):
		self.nodes = {}
	
	def add_node(self, node_name, matcher):
		self.nodes[node_name] = (matcher)
