import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '../../src'))

from compiler.parser import *

def test(line, matcher, eof=False):
	print("#" * 25)
	print(f"line:\n{line}\n")
	print(f"matcher:\n{matcher}\n")
	tokens = tokenize(line)
	print(f"tokens:\n{tokens}\n")
	match, cst_elements, _ = matcher.parse_tokens(tokens, True)
	print(f"match:\n{match}\n")
	if type(cst_elements) == list:
		print(f"cst_elements:\n" + "\n".join(str(element) for element in cst_elements) + "\n")
	else:
		print(f"cst_node:\n" + str(cst_elements) + "\n")
	print()


class CST_Node:
	def __init__(self, name, children):
		self.name = name
		self.children = children
		self.params = [
			child
			for child in self.children
			if isinstance(child, CST_Node)
		]

	def __str__(self):
		return self.name + "".join(
			("\n|-- ", "\n`-- ")[i == len(self.children)-1] + "".join(
				("|   ", "    ")[i == len(self.children)-1].join(
					str(child).splitlines(True)
				)
			)
			for i, child in enumerate(self.children)
		)


class CST_plus(CST_Node):
	def __init__(self, _, children):
		super().__init__("plus", children)

graph = ParsingGraph(None)
graph.add_node(
	"plus", CST_plus,
	MatchAny(
		MatchSequence(
			MatchTokenContent("+"),
			MatchParsingNode(graph, "plus"),
			MatchTokenContent("+")
		),
		MatchTokenContent("+")
	),
	[]
)

test("+++++", graph.nodes["plus"])