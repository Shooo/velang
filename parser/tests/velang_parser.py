import sys
sys.path.append("..")
sys.path.append("..")
sys.path.append("src")

from compiler.tokenizer import *
from compiler.parser import *
from velang.syntax_declaration import *

def interpreter():
	graph = velang_syntax()
	while True:
		content = input(">>> ")
		tokens = tokenize(content)
		match, cst_node, _ = graph.nodes["statement"].parse_tokens(tokens, True)
		if not match:
			print("Couldn't parse this statement (invalid syntax)")
		else:
			print(cst_node)

def tokenize_file(path):
	graph = velang_syntax()
	with open(path) as file:
		content = file.read()
		tokens = tokenize(content)
		print(tokens)
		match, cst_node, _ = graph.nodes["script"].parse_tokens(tokens)
		if not match:
			print("Couldn't parse this file (invalid syntax)")
		else:
			print(cst_node)

if __name__ == "__main__":
	import sys

	args = sys.argv[1:]
	if len(args) == 0:
		interpreter()
	elif len(args) == 1:
		tokenize_file(args[0])
	else:
		print("Too many arguments. You should either enter a file path, or nothing.")
	 
