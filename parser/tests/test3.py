import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '../../'))

from tokenizer.tokenizer import tokenize
from parser.parser import *

line = \
"""25 * 2 * 5 * 6"""

tokens = tokenize(line)
print(tokens)

graph = ParsingGraph()
graph.add_node(
	"expression",
	MatchAny(
		MatchSequence(
			MatchParsingNode(
				graph,
				"expression"
			),
			MatchTokenContent(
				"*"
			),
			MatchTokenType(
				"NUMBER"
			)
		),
		MatchTokenType(
			"NUMBER"
		)
	)
)

match_result = MatchParsingNode(graph, "expression").parse_tokens(tokens, True)
print(f"Match: {match_result.result}")
print("tree:\n" + str(match_result.elements))
